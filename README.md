# Web Scraper

This application uses Node to log in to a IMDb account and scrapes a private list of movies.

## Installation


```bash
npm install
```

## Usage

When the application is executed it uses the Node module Nightmare to fetch the private list from IMDb. The HTML data is then written into movies.txt.

The scraping is done using Cheerio and it loads the content of movies.txt. The fetching with Nightmare can, therefore, be skipped when testing the Cheerio-scraping part. To do this, uncomment the first 3 commands in the start() function as:

```
async function start () {
  // const response = await scraper.loginAndFetchData()
  // console.log('Fetching data complete.')

  // writeToTextFile(response)

  ...
}
```