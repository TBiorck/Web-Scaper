/**
 * Fetch movie data from the /movies API.
 */
async function getMovies () {
  const response = await fetch('/movies')
  const json = await response.json()

  addMoviesToHtml(json)
}

/**
 * Add the data to the HTML using the template defined in index.html.
 * @param {JSON} movies
 */
function addMoviesToHtml (movies) {
  const moviesContainer = document.getElementById('movies-container')
  const template = document.getElementById('movie-template')

  movies.forEach(movie => {
    // Clone the movie template
    const clone = template.content.cloneNode(true)

    // Set movie title
    const title = clone.querySelector('h3')
    title.innerText = movie.title

    // The <p> elements are (in order): year, genre, rating, summary and director
    const data = clone.querySelectorAll('p')
    data[0].innerText = movie.year
    data[1].innerText = movie.genre
    data[2].innerText = `Rating: ${movie.rating}`
    data[3].innerText = movie.summary
    data[4].innerText = `Director: ${movie.director}`

    // Add the actors to a list
    const list = clone.querySelector('ul')
    movie.actors.forEach(actor => {
      const li = document.createElement('li')
      li.innerText = actor

      list.appendChild(li)
    })

    // Set image
    const img = clone.querySelector('img')
    img.src = movie.image

    // Finally add the movie to the movies container
    moviesContainer.appendChild(clone)
  })
}
