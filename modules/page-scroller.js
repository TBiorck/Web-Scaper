
/**
 * This JavaScript code is injected on the target site using Nightmare. It
 * scrolls down the page in order for the content to properly load.
 */

let currentHeight = 0

while (currentHeight <= document.body.scrollHeight) {
  currentHeight += 100
  window.scrollTo({
    top: currentHeight,
    left: 0,
    behavior: 'smooth'
  })
}

// Append an identifiable div to the body of the html to let Nightmare know when
// the script has finished.
const div = document.createElement('div')
div.setAttribute('id', 'scroll-finish')
document.body.appendChild(div)
