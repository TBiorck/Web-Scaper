module.exports = {
  loginAndFetchData: loginAndFetchData,
  scrapeTextFile: scrapeTextFile
}

const cheerio = require('cheerio')
const Nightmare = require('nightmare')
const nightmare = Nightmare({ show: true })
const path = require('path')
console.log(path.join(__dirname, 'page-scroller.js'))

/**
 * Logs into IMDb.com to access a private list of movies and fetch HTML code.
 * @returns {Promise} movies - Returns a promise of the specific HTML code containing the list of movies as a string.
 */
function loginAndFetchData () {
  console.log('Fetching data..')
  return nightmare
    .goto('https://www.imdb.com/ap/signin?openid.pape.max_auth_age=0&openid.return_to=https%3A%2F%2Fwww.imdb.com%2Fregistration%2Fap-signin-handler%2Fimdb_us&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.assoc_handle=imdb_us&openid.mode=checkid_setup&siteState=eyJvcGVuaWQuYXNzb2NfaGFuZGxlIjoiaW1kYl91cyIsInJlZGlyZWN0VG8iOiJodHRwczovL3d3dy5pbWRiLmNvbS9saXN0L2xzMDgxNjc4MDI1Lz9yZWZfPWxvZ2luIn0&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&tag=imdbtag_reg-20')
    .type('#ap_email', 'timothy.biorck@se.experis.com')
    .type('#ap_password', 'thisismypassword')
    .click('#signInSubmit')
    .wait('#rvi-div') // This div is close to the end of the html. Wait for it to load before proceeding.
    .inject('js', path.join(__dirname, 'page-scroller.js')) // Inject script for scrolling down the page. Required for loading the images.
    .wait('#scroll-finish')
    .wait(3000) // Wait to allow images to be loaded
    .evaluate(() => {
      // Get the movies container by classname
      const moviesContainer = document.querySelector('.lister-list').innerHTML

      return moviesContainer
    })
    .end()
    .then(movies => {
      console.log('Fetching data complete.')
      return movies
    })
    .catch(error => {
      console.log('Search failed:', error)
    })
}

/**
 * Scrapes the content of input 'data' for each movie's data.
 * @param {string} data - The movie-list HTML code as a string.
 * @returns {Array} moviesData - An array of movie objects.
 */
function scrapeTextFile (data) {
  console.log('Scraping data..')
  const $ = cheerio.load(data)

  // Get the elements that have both the classes 'lister-item' and 'mode-detail'
  const movies = $('.lister-item.mode-detail')

  const moviesData = []

  $(movies).each((index, movie) => {
    // Save each movie's data into this json object
    const movieObject = {}

    /* TITLE AND YEAR */
    const header = $(movie).find('.lister-item-header')
    movieObject.title = getTitle($(header))
    movieObject.year = getYear($(header))

    /* GENRE AND RATING */
    const content = $(movie).find('.lister-item-content')
    movieObject.genre = getGenre($(content))
    movieObject.rating = getRating($(content))

    /* SUMMARY */
    const summaryTag = $(content).find('.inline-block.ratings-metascore').next()
    movieObject.summary = getSummary($(summaryTag))

    /* DIRECTOR AND ACTORS */
    const directorAndActorTag = $(summaryTag).next()
    movieObject.director = getDirector($(directorAndActorTag))
    movieObject.actors = getActors($(directorAndActorTag), $)

    /* IMAGE */
    movieObject.image = getImage($(movie))

    // Finally add the movie to the array of movie objects
    moviesData.push(movieObject)
  })

  console.log('Scraping complete.')
  return moviesData
}

/* SCRAPE SPECIFIC DATA FUNCTIONS */

function getTitle (header) {
  return header.find('a').text()
}

function getYear (header) {
  return header.find('.lister-item-year.text-muted.unbold').text().replace('(', '').replace(')', '')
}

function getGenre (content) {
  // Remove line break and whitespace then append space after each comma
  return content.find('.genre').text().replace(/\r?\n|\r\s/g, '').replace(/\s/g, '').split(',').join(', ')
}

function getRating (content) {
  return content.find('.ipl-rating-star.small span.ipl-rating-star__rating').text()
}

function getImage (movie) {
  return movie.find('.lister-item-image img').attr('src')
}

function getSummary (summary) {
  const summaryWithSpace = summary.text().replace(/\r?\n|\r\s/g, '').split('    ').join(' ') // Remove line break and large whitespace (creates a space as first char of string)
  return summaryWithSpace.substring(1, summaryWithSpace.length) // Remove space in first char of string
}

function getDirector (directorAndActor) {
  return directorAndActor.children().first().text()
}

function getActors (directorAndActor, $) {
  const directorIndex = 0
  const actors = []

  directorAndActor.find('a').each((index, person) => {
    if (index !== directorIndex) {
      actors.push($(person).text())
    }
  })
  return actors
}
