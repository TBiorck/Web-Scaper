const fs = require('fs')
const path = require('path')
const scraper = require('./modules/scraper.js')
const express = require('express')
const app = express()

start()

/**
 * Starts the application by fetching data and storing it into a txt file, then scrape
 * the content and write result into a json file. Finally start the server making the data
 * accessible on localhost:8080.
 */
async function start () {
  const response = await scraper.loginAndFetchData()

  await writeToTextFile(response)

  const file = await fs.promises.readFile(path.join(__dirname, 'movies.txt'), 'utf8')
  const moviesData = scraper.scrapeTextFile(file)

  await writeToJsonFile(moviesData)

  startServer()
}

/**
 * Write the content of 'movies' into the file 'movies.txt'.
 * @param {string} movies - A string containing the list of movies as HTML.
 */
async function writeToTextFile (movies) {
  console.log('Writing to text file..')
  await fs.promises.writeFile(path.join(__dirname, 'movies.txt'), movies)
  console.log('Writing to text file completed.')
}

/**
 * Write the content of 'movies' into the file 'movies.json' then start express server.
 * @param {Array} movies - An array of movie objects.
 */
async function writeToJsonFile (moviesData) {
  console.log('Writing to json file..')
  await fs.promises.writeFile(path.join(__dirname, 'movies.json'), JSON.stringify(moviesData, null, 2))
  console.log('Writing to json file completed.')
}

function startServer () {
  // Set up serving of static content
  app.use('/assets', express.static(path.join(__dirname, 'public', 'static')))

  // Endpoints
  app.get('/', (req, res) => {
    return res.sendFile(path.join(__dirname, 'public', 'index.html'))
  })

  app.get('/movies', (req, res) => {
    return res.sendFile(path.join(__dirname, 'movies.json'))
  })

  // Start listening
  app.listen(8080, () => console.log('Server is listening on port: 8080'))
}
